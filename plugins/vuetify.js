import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: "#29B6F6",
    secondary: "#EF5350",
    accent: "#FBC02D",
    error: "#f44336",
    warning: "#ffeb3b",
    info: "#2196f3",
    success: "#7CB342"
  }
})
