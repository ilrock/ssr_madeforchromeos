import mixpanel from "mixpanel-browser";
import Vue from 'vue'

mixpanel.init(process.env.mixpanelId, {
  debug: true
});

Vue.prototype.$mixpanel = mixpanel