require('dotenv').config()
const pkg = require('./package')

module.exports = {
  mode: 'universal',
  env: {
    mixpanelId: process.env.VUE_APP_MIXPANEL_ID
  },
  server: {
    port: 8000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Made for ChromeOs',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Made for ChromeOs is the first crowdsourced list of the best Android apps for ChromeOs and Chromebooks' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '~assets/images/logo.png' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.0.13/css/all.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Istok+Web' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    { src: "@/plugins/mixpanel", ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/dotenv',
    [
      '@nuxtjs/google-analytics', {
        id: 'UA-130194215-1'
      }
    ],
    [
      'nuxt-fire', {
        config: {
          development: {
            apiKey: process.env.VUE_APP_API_KEY,
            authDomain: process.env.VUE_APP_AUTH_DOMAIN,
            databaseURL: process.env.VUE_APP_DB_URL,
            projectId: process.env.VUE_APP_PROJECT_ID,
            storageBucket: process.env.VUE_APP_TORAGE_BUCKET,
            messagingSenderId: process.env.VUE_APP_SENDER_ID
          },
          production: {
            apiKey: process.env.VUE_APP_API_KEY,
            authDomain: process.env.VUE_APP_AUTH_DOMAIN,
            databaseURL: process.env.VUE_APP_DB_URL,
            projectId: process.env.VUE_APP_PROJECT_ID,
            storageBucket: process.env.VUE_APP_TORAGE_BUCKET,
            messagingSenderId: process.env.VUE_APP_SENDER_ID
          }
        }
      }
    ],
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      
    }
  }
}
