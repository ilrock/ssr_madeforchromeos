import Vuex from 'vuex'
import firebase from 'firebase/app'
import Cookies from 'js-cookie'
import {getUserFromCookie, getUserFromSession} from '@/helpers'

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      apps: [],
      currentUser: null,
      showLoginModal: false,
      showCreateAppModal: false
    }),
    getters: {
      apps (state) {
        const apps = state.apps.sort((a,b) => b.upvotes - a.upvotes)
        return apps
      },
      appsByTitle: state => query => {
        const apps = state.apps.sort((a,b) => b.upvotes - a.upvotes).filter(app => app.title.toLowerCase().indexOf(query.toLowerCase()) >= 0)
        return apps
      },
      currentUser (state) {
        return state.currentUser
      },
      showLoginModal (state) {
        return state.showLoginModal
      },
      showCreateAppModal (state) {
        return state.showCreateAppModal
      }
    },
    mutations: {
      setApps (state, payload) {
        state.apps = payload
      },
      setUser (state, payload) {
        state.currentUser = payload
      },
      setShowLoginModal (state, payload) {
        state.showLoginModal = payload
      },
      setShowCreateAppModal (state, payload) {
        state.showCreateAppModal = payload
      }
    },
    actions: {
      nuxtServerInit ({ commit }, { req }) {        
        const user = getUserFromCookie(req)
        if (user) {
          console.log(user)
          commit('setUser', {
            name: user.name,
            id: user.user_id,
            email: user.email
          })
          // await commit('modules/user/setUSER', { name: user.name, email: user.email, avatar: user.picture, uid: user.user_id})
        }
        return new Promise ((resolve, reject) => {
          this.$fireStore.collection('apps').onSnapshot((snap) => {
            const apps = snap.docs.map((doc) => {
              return doc.data()
            })
            commit ('setApps', apps)
            resolve()
          })
        })
      },
      signin ({ commit }) {
        return new Promise((resolve, reject) => {
          const provider = new firebase.auth.GoogleAuthProvider()
          firebase.auth().signInWithPopup(provider)
            .then((result) => {

              this.$fireAuth.currentUser.getIdToken(true)
                .then((token) => {
                  Cookies.set('access_token', token)
                })     
                       
              const user = {
                id: result.user.uid,
                email: result.user.email,
                name: result.user.displayName
              }
    
              commit('setUser', user)
              resolve(user)
            })
            .catch((err) => console.log(err))
        })
      },
      setShowLoginModal ({ commit }, payload) {
        commit('setShowLoginModal', payload)
      },
      setShowCreateAppModal ({ commit }, payload) {
        commit('setShowCreateAppModal', payload)
      },
      getApps ({ commit, state }) {
        return this.$fireStore.collection('apps').get()
          .then((res) => {
            const apps = res.docs.map((doc) => doc.data()).sort((a,b) => b.upvotes - a.upvotes)
            commit('setApps', apps)
          })
      },
      createApp ({ dispatch, state }, payload) {
        const app = payload
        if (!app.video)
          app.video = ''
  
        if (!app.videoImage)
          app.videoImage = ''

        if (!app.developerWebsite)
          app.developerWebsite = ''

        app.user = state.currentUser || { name: 'Excel Sheet'}
        app.upvotes = 0
        app.upvoters = []
        app.downvoters = []
        
        this.$fireStore.collection('apps').doc(app.id).set(app)
        return Promise.resolve()
      },
      upvote ({ state }, payload) {
        const user = state.currentUser
        const app = payload
  
        if (app.upvoters.indexOf(user.id) === -1) {
          return this.$fireStore.collection('apps').doc(app.id).update({
            upvoters: firebase.firestore.FieldValue.arrayUnion(user.id),
            upvotes: app.upvotes + 1
          })
        }
      },
      downvote ({ state }, payload) {
        const user = state.currentUser
        const app = payload
        
        if (!app.downvoters) {
          return this.$fireStore.collection('apps').doc(app.id).update({
            downvoters: [user.id],
            upvotes: app.upvotes - 1
          })
        } else if (app.downvoters.indexOf(user.id) === -1) {
          return this.$fireStore.collection('apps').doc(app.id).update({
            downvoters: firebase.firestore.FieldValue.arrayUnion(user.id),
            upvotes: app.upvotes - 1
          })
        }
      }
    }
  })
}

export default createStore